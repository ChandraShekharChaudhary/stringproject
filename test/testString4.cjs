let fullNameinTitleCase=require('../string4.cjs');

let nameObj1 = { "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" };
let nameObj = { "first_name": "JoHN", "last_name": "SMith" };

console.log(fullNameinTitleCase(nameObj));
console.log(fullNameinTitleCase(nameObj1));
console.log(fullNameinTitleCase({"firstName": "ram"}));
console.log(fullNameinTitleCase({"first_name" : "prem", "last_name": "ChoPara"}));
console.log(fullNameinTitleCase({"first_name" : "Me", "last_name": "myselF"}));
console.log(fullNameinTitleCase({"first_name": "ram", "last_name": "deEn"}));
console.log(fullNameinTitleCase({"first_name": "r", "last_name": "deEn"}));
