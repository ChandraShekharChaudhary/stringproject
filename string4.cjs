// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const fullNameinTitleCase=(nameObj)=>{
    let name = "";
    let nameArr=[];

    for (let key in nameObj) {
        
        let namePart=nameObj[key];
        let fristChar=namePart.charAt(0).toUpperCase();
        let restChar=namePart.substr(1,namePart.length).toLowerCase();
        name = name + ' ' + fristChar+restChar;

    }
    name=name.trim();
    return name;
}
module.exports=fullNameinTitleCase;