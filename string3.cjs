// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

// let dateStr="10/15/2021";

function findMonth(dateStr) {
    let datePartArr = dateStr.split('/');
    if (datePartArr[1] > 12 || datePartArr[1] <= 0 || isNaN(datePartArr[1]) || datePartArr.length !== 3) {
        return "NaD";
    }
    let monthName = ["", "January", "February", "March", "April",
        "May", "June", "July", "August",
        "September", "October", "November", "December"];
    return monthName[datePartArr[1]];
}

module.exports=findMonth;