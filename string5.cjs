// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

const arrayToString=(arrayOfString)=>{
   
    let str="";
    for(let index=0; index<arrayOfString.length; index++){
        str=str+" "+arrayOfString[index];
    }
    str.trim();
    return str;
}
module.exports=arrayToString;
