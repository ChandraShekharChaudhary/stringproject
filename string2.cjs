// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.


//let ipAddress="1111.139.161.143";


function returnIPArray(ipAddressStr){
    let ipPartArray=ipAddressStr.split('.');  // spliting all part of the IP address according dot presence.
    if(ipPartArray.length !== 4){  // check the part of IP address.
        return [];
    }
    for(let index=0 ; index<ipPartArray.length; index++){
        if(ipPartArray[index]===''){     //checking if two consecutive dots, which is not allowed in IP addresses.
            return [];
        }
        ipPartArray[index]=+ipPartArray[index]; //converting all part in number.
        if(ipPartArray[index]>=256 || ipPartArray[index]<0 || isNaN(ipPartArray[index])){  //checking valid number range and is it a number.
            return [];
        }
    }
    return ipPartArray;
}

module.exports=returnIPArray;